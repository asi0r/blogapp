import rosatiPhoto from './rosati.jpg';
import ostrowskaPhoto from './ostrowska.jpg';
import szroederPhoto from './szroeder.jpg';

export const importedPhotos = {
    ostrowska: ostrowskaPhoto,
    rosati: rosatiPhoto,
    szroeder: szroederPhoto

}