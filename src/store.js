import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux'

import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import rootReducer from './reducers'

export const history = createHistory()

const initialState = window.initialReduxState;
const middleware = [thunk, routerMiddleware(history)]

const composedEnhancers = compose(
  applyMiddleware(...middleware)
)

export default createStore(
  rootReducer,
  initialState,
  composedEnhancers
)


