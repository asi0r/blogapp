import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from './App';
import { Provider } from 'react-redux';
import store, { history } from './store'
import { Router } from 'react-router';

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
        <App />
    </Router>
  </Provider>,
  document.getElementById("root"))

