import React from 'react';

const Comment = props => {
  return (
    <div className="comment">
        <h3 className="nick">{props.nick}</h3>
        <p className="content">{props.content}</p>
    </div>
  );
};
export default Comment;