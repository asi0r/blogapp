import React, { Component } from "react";
import Post from "./Post";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

class MainSide extends Component {
  state = {
    deleteAll: false
  };

  changeDeleteAll = () => {
    this.setState({ deleteAll: !this.state.deleteAll });
  };

  render() {
    let posts = this.props.posts.map(post => (
      <Post
        key={post.id}
        post={post}
        click={() => this.handleAddContent(post.id)}
        deleteModeOff={this.state.deleteAll}
      />
    ));

    return (
      <div>
        <button
          className="addPost"
          onClick={() => this.props.history.push("/NewPost")}
        >
          Dodaj nowy post
        </button>
        <div className="deleteAll">
          <label>
            Szybkie usuwanie
            <input
              type="checkbox"
              checked={this.state.deleteAll}
              onChange={this.changeDeleteAll}
            />
          </label>
        </div>
        <h1>Strona główna</h1>
        <h3>{posts}</h3>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts
});

export default withRouter(connect(mapStateToProps)(MainSide));
