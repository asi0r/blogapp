import React, { Component } from 'react';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { deletePost } from '../actions/blog';

class Post extends Component {
  navigateTo = id => {
    this.props.history.push(`/Post/${id}`);
  };

  handleDeletePost = () => {
    this.props.deletePost(this.props.post.id);
  };

  render() {
    const { id, title, photo } = this.props.post;
    const fastDelete = 'X';
    return (
      <div
        className="post"
        onClick={() => !this.props.deleteModeOff && this.navigateTo(id)}
      >
        {this.props.deleteModeOff ? (
          <div className="postX" onClick={this.handleDeletePost}>
            {fastDelete}
          </div>
        ) : null}
        <h2>{title}</h2>
        <div className="photo">
          <img src={photo} alt="hero of this post" />
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  posts: state.posts.posts
});
const mapDispatchToProps = dispatch => ({
  deletePost: post => dispatch(deletePost(post))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Post)
);
