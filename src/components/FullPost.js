import React from 'react';
const FullPost = (props) => {
    return ( 
        <div>
            <h2>{props.title}</h2>
            <div className="photo">
            < img src={props.photo} alt = "hero of this post"/>
            </div>
            <p>{props.content}</p>
        </div>
     );
}
 
export default FullPost;