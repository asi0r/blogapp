import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { editPost } from './../actions/blog';

class EditPost extends Component {
  state = {
    titleValue: this.props.posts[this.props.match.params.id - 1].title,
    linkValue: this.props.posts[this.props.match.params.id - 1].photo,
    contentValue: this.props.posts[this.props.match.params.id - 1].content,
    isValid: false
  };
  handleGetTitle = e => {
    this.setState({ titleValue: e.target.value });
  };
  handleGetContent = e => {
    this.setState({ contentValue: e.target.value });
  };
  handleGetLink = e => {
    this.setState({ linkValue: e.target.value });
  };
  handleEditPost = async () => {
    let id = parseInt(this.props.match.params.id, 10);
    const post = {
      id: id,
      title: this.state.titleValue,
      photo: this.state.linkValue,
      content: this.state.contentValue
    };

    await this.checkFormValid(post);
    if (this.state.isValid) {
      this.props.editPost(post);
      this.props.history.push('/');
    }
  };

  checkFormValid(post) {
    if (post.content === '' || post.photo === '' || post.title === '') {
      this.notificationError('All fields must be filled');
      return;
    }
    if (!post.photo.includes('http')) {
      this.notificationError('wrong link to photo');
      return;
    }
    this.setState({ isValid: true });
  }

  notificationError(text) {
    NotificationManager.error(text, 'Click me!', 5000, () => {
      alert('callback');
    });
  }

  render() {
    return (
      <div className="newPost">
        <label className="label">
         Tytuł postu:
          <input
            type="text"
            value={this.state.titleValue}
            onChange={this.handleGetTitle}
          />
        </label>
        <br />
        <label className="label">
          {' '}
          Link do zdjęcia:
          <input
            type="text"
            value={this.state.linkValue}
            onChange={this.handleGetLink}
          />
        </label>
        <br />
        <label className="label3">
          {' '}
         Pełny tekst postu:
          <textarea
            value={this.state.contentValue}
            onChange={this.handleGetContent}
          />
          <br />
        </label>
        <button className="addNewPost" onClick={() => this.handleEditPost()}>
          Zapisz zmiany
        </button>
        <button className="back" onClick={() => this.props.history.push('/')}>
          Powrót
        </button>
        <NotificationContainer />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  posts: state.posts.posts
});
const mapDispatchToProps = dispatch => ({
  editPost: post => dispatch(editPost(post))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(EditPost)
);
