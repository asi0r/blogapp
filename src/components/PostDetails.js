import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { deletePost } from '../actions/blog';
import Comment from './Comment';
import AddComment from './AddComment';

class PostDetails extends Component {
  navigateTo = id => {
    this.props.history.push(`/EditPost/${id}`);
  };
  handleDeletePost = () => {
    const id = parseInt(this.props.match.params.id, 10);
    this.props.deletePost(id);
    this.props.history.push('/');
  };

  render() {
    const post = this.props.posts.find(p => p.id === parseInt(this.props.match.params.id, 10));
    const comments = post.comments === undefined ? null : (post.comments.map(c => (
      <Comment key={c.id} id={c.id} content={c.content} nick={c.nick} />))) 
    return (
      <div className="postDetails">
        <button className="back" onClick={() => this.props.history.push('/')}>
         Powrót
        </button>
        <div className="post">
          <h2>{post.title}</h2>
          <div className="photo">
            <img src={post.photo} alt="hero of this post" />
            <p>{post.content}</p>
          </div>
          <div className="delete">
            <button onClick={this.handleDeletePost}>Usuń</button>
          </div>
          <div className="edit">
            <button onClick={() => this.navigateTo(this.props.match.params.id)}>
              Edytuj
            </button>
          </div>
        </div>
        <AddComment />
        {comments}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts,
});
const mapDispatchToProps = dispatch => ({
  deletePost: post => dispatch(deletePost(post)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(PostDetails),
);
