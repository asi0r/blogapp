import React from 'react';
import NewPost from '..//NewPost';
import MainSite from '../MainSite'
import PostDetails from '..//PostDetails';
import "./Blog.css";
import { Switch, Route } from 'react-router';
import EditPost from './../EditPost';

export const Blog = () => {
  return (
    <Switch>
      <Route exact path="/" component={MainSite} />
      <Route path="/NewPost" component={NewPost} />
      <Route path="/Post/:id" component={PostDetails} />
      <Route path="/EditPost/:id" component={EditPost} />
    </Switch>
  );
};


export default Blog;