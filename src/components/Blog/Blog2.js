import React, { Component } from 'react';
import PostsList from './PostsList';
import NewPosts from './NewPost';
import MainSide from './MainSide';
import './Blog.css'
import FullPost from './FullPost';

class Blog extends Component {
    state = {
        posts: [
            {id: 1, title:"Rosati straciła duży kontrakt reklamowy. Przez Śmigielskiego?", photo:"https://i.wpimg.pl/623x461/f5.pudelek.pl/a695357c84daed2197ce18e9370ae0b4e2071781.jpg", content: "Choć Weronika Rosati stara się, by kojarzono ją przede wszystkim z aktorstwem i pracą w Ameryce, brutalna prawda obnaża fakty. Aktualnie Weronika nie jest odnoszącą największe sukcesy za oceanem polską aktorką, a w kraju mówi się o niej przede wszystkim w związku z problemami w życiu prywatnym."  },
            {id: 2, title:"Ilona Ostrowska zmieniła fryzurę", photo: "http://i1.pudelekx.pl/8025d84e976434c7396c7321f7187ef5efd54ebc/x_ostrowska-png", content: "Zima to trudny okres dla ambitnych polskich celebrytek, które nie lubią tracić okazji na zaprezentowanie się na ściance. Niskie temperatury, porywisty wiatr czy śnieg nie sprzyjają kusym kreacjom i misternie ułożonym fryzurom."},
            {id: 3, title: "Natalia Szroeder popija soczek",photo: "http://i1.pudelekx.pl/8c7072391cda7283f8315918f1282d8be6a2f4d8/szreder_pije-png", content: ""}
        ],
        click: false,
        backClick: false
     }
     handleBooleanChange=() => {
        this.setState({
            click: !this.state.click,
        })
     }
     addPost=(post)=> {
        let posts= this.state.posts.concat(post);
        console.log(posts )
        this.setState({posts , click: !this.state.click });
     }
   backToMainSite = () => {
        this.setState({backClick: !this.state.backClick})
   }
   handleAddContent=(id) => {
       let posts= this.state.posts;
       const postId= posts.find(post=> post.id === id)
       console.log(postId, "id")
}
    render() { 
        let posts= this.state.posts;
        posts= posts.map(post => <PostsList key={post.id} title={post.title} photo={post.photo} content={post.content} click={() => this.handleAddContent(post.id)} />)
        return ( 
            <div>
               {this.state.click ? <NewPosts addPost={this.addPost} posts={this.state.posts} click={this.backToMainSite}/> : <MainSide posts={posts} click={this.handleBooleanChange} addContent={this.handleAddContent(posts.id)}/>
               }
               {/* <FullPost posts={posts[id]}/> */}
               {/* {this.state.backClick &&  <MainSide posts={posts} click={this.handleBooleanChange}/> } */}
            </div>
         );
    }
}
 
export default Blog;