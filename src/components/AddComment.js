import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { addComment } from '../actions/blog';

class AddComment extends Component {
  state = {
    contentValue: '',
    nickValue: '',
    buttonClick: false
  };
  handleNickValueChange = e => {
    this.setState({ nickValue: e.target.value });
  };
  handleContentValueChange = e => {
    this.setState({ contentValue: e.target.value });
  };
  handleAddComment = () => {
    const id = parseInt(this.props.match.params.id, 10);
    const postComment = {
      postId: id,
      commentNick: this.state.nickValue,
      commentContent: this.state.contentValue
    };
    if(postComment.commentNick !== '' && postComment.commentContent !== '' ){
      this.props.addComment(postComment) 
      this.setState({
        contentValue: "",
        nickValue: ""
      })
    }
    else{
      alert('uzupełnij wszystkie pola')
    };
  };
  handleChangeButtonClick = () => {
    this.setSctate({ buttonClick: !this.state.buttonClick });
  };
  render() {
    return (
      <div className="addComment">
        <label>
          Podaj swój nick:
          <input
            type="text"
            value={this.state.nickValue}
            onChange={this.handleNickValueChange}
          />
        </label>
        <label>
          Podaj treść komentarza:
          <textarea
            value={this.state.contentValue}
            onChange={this.handleContentValueChange}
          />
        </label>
        <button className="addButton" onClick={() => this.handleAddComment()}>
          Dodaj komentarz
        </button>
        <button className="back" onClick={() => this.props.history.push('/')}>
          Powrót
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts
});
const mapDispatchToProps = dispatch => ({
  addComment: comment => dispatch(addComment(comment))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddComment)
);
