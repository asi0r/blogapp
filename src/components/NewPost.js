import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  NotificationManager,
  NotificationContainer
} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { addNewPost } from '../actions/blog';

class NewPost extends Component {
  state = {
    titleValue: '',
    linkValue: '',
    contentValue: '',
    isValid: false
  };

  handleGetTitle = e => {
    this.setState({ titleValue: e.target.value });
  };
  handleGetContent = e => {
    this.setState({ contentValue: e.target.value });
  };
  handleGetLink = e => {
    this.setState({ linkValue: e.target.value });
  };

  handleAddNewPost = () => {
    const post = {
      title: this.state.titleValue,
      photo: this.state.linkValue,
      content: this.state.contentValue
    };
    if (this.formIsValid(post)) {
      this.props.addNewPost(post);
      this.props.history.push('/');
    }
  };

  formIsValid(post) {
    if (post.content === '' || post.photo === '' || post.title === '') {
      this.notificationError('All fields must be filled');
      return false;
    }
    if (!post.photo.includes('http')) {
      this.notificationError('wrong link to photo');
      return false;
    }
    return true;
  }
  notificationError(text) {
    NotificationManager.error(text, 'Click me!', 5000, () => {
      alert('callback');
    });
  }

  render() {
    return (
      <div className="newPost">
        <label className="label">
           Tytuł postu:
          <input
            type="text"
            value={this.state.titleValue}
            onChange={this.handleGetTitle}
          />
        </label>
        <br />
        <label className="label">
          {' '}
           Link do zdjęcia:
          <input
            type="text"
            value={this.state.linkValue}
            onChange={this.handleGetLink}
          />
        </label>
        <br />
        <label className="label3">
          {' '}
          Pełny tekst postu:
          <textarea
            value={this.state.contentValue}
            onChange={this.handleGetContent}
          />
          <br />
        </label>
        <button className="addNewPost" onClick={() => this.handleAddNewPost()}>
          Dodaj nowy post
        </button>
        <button className="back" onClick={() => this.props.history.push('/')}>
          Powrót
        </button>
        <NotificationContainer />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  posts: state.posts.posts
});
const mapDispatchToProps = dispatch => ({
  addNewPost: post => dispatch(addNewPost(post))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(NewPost)
);
