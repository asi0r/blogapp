import React, { Component } from 'react';
import { Blog } from './components/Blog';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="wrap">
        <Blog />
      </div>
    );
  }
}

export default App;
