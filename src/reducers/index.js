import { combineReducers } from 'redux';
import posts from './blog';
import { routerReducer } from 'react-router-redux';

export default combineReducers ({
  posts,
  router: routerReducer
})