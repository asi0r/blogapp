import {
  ADD_NEW_POST,
  DELETE_POST,
  EDIT_POST,
  ADD_COMMENT
} from '../constans/blog';

import { importedPhotos } from "../assets/index.js";

const initialState = {
  posts: [
    {
      id: 1,
      title: 'Rosati straciła duży kontrakt reklamowy. Przez Śmigielskiego?',
        photo: importedPhotos.rosati,
      content:
        'Choć Weronika Rosati stara się, by kojarzono ją przede wszystkim z aktorstwem i pracą w Ameryce, brutalna prawda obnaża fakty. Aktualnie Weronika nie jest odnoszącą największe sukcesy za oceanem polską aktorką, a w kraju mówi się o niej przede wszystkim w związku z problemami w życiu prywatnym.',
      comments: [
        {
          id: 1,
          nick: 'blogerka',
          content: 'Kto to jest Smiegielski?'
        },
        {
          id: 2,
          nick: 'Grażyna',
          content: 'cos jej się przytyło w tej Ameryce'
        }
      ]
    },
    {
      id: 2,
      title: 'Ilona Ostrowska zmieniła fryzurę',
        photo: importedPhotos.ostrowska,
      content:
        'Zima to trudny okres dla ambitnych polskich celebrytek, które nie lubią tracić okazji na zaprezentowanie się na ściance. Niskie temperatury, porywisty wiatr czy śnieg nie sprzyjają kusym kreacjom i misternie ułożonym fryzurom.',
      comments: [
        {
          id: 1,
          nick: 'Janusz',
          content: 'Kto ją tak obciął?'
        },
        {
          id: 2,
          nick: 'Sebek',
          content: 'Piękna fryzura <3 Tak trzymaj Ilona'
        }
      ]
    },
    {
      id: 3,
      title: 'Natalia Szroeder popija soczek',
      photo: importedPhotos.szroeder,
      content: '',
      comments: [
        {
          id: 1,
          nick: 'Gość',
          content: 'Fajne kwiatki'
        }
      ]
    }
  ]
};

export default function posts(state = initialState, action) {
  switch (action.type) {
    case ADD_NEW_POST: {
      const posts = state.posts;
      const newPost = action.payload;
      newPost.id = posts.length + 1;
      return {
        ...state,
        posts: posts.concat(newPost)
      };
    }
    case DELETE_POST: {
      const idPostToDelete = action.payload;
      const allPosts = state.posts;
      const newPosts = allPosts.filter(post => post.id !== idPostToDelete);
      return {
        ...state,
        posts: newPosts
      };
    }

    case EDIT_POST: {
      const statePosts = state.posts;
      const editPost = action.payload;
      const indexPostBeforeEdit = statePosts.indexOf(postBeforeEdit);
      const postBeforeEdit = statePosts.find(p => p.id === editPost.id);
      postBeforeEdit.title = editPost.title;
      postBeforeEdit.photo = editPost.photo;
      postBeforeEdit.content = editPost.content;
      statePosts[indexPostBeforeEdit] = postBeforeEdit;
      return {
        ...state,
        posts: statePosts
      };
    }

    case ADD_COMMENT: {
      let post = state.posts.find(p => p.id === action.payload.postId);
      const arrayIndex = state.posts.indexOf(post);
      const newComment = {
        id: post.comments ? post.comments.length + 1 : 1,
        nick: action.payload.commentNick,
        content: action.payload.commentContent
      };
      if (post.comments === undefined) {
        post = {...post, comments: []};
      }
      console.log('post', post);
      console.log('new comments',newComment)
      post.comments.push(newComment);
      const newPosts = state.posts;
      newPosts[arrayIndex] = post;
      return {
        ...state,
        posts: [...newPosts]
      };
    }
    default:
      return state;
  }
}
