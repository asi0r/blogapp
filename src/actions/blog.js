import {
  ADD_NEW_POST,
  DELETE_POST,
  EDIT_POST,
  ADD_COMMENT
} from '../constans/blog';

export const addNewPost = post => {
  return dispatch => {
    dispatch({
      type: ADD_NEW_POST,
      payload: post
    });
  };
};
export const deletePost = post => {
  return dispatch => {
    dispatch({
      type: DELETE_POST,
      payload: post
    });
  };
};

export const editPost = post => {
  return dispatch => {
    dispatch({
      type: EDIT_POST,
      payload: post
    });
  };
};

export const addComment = comment => dispatch => {
  dispatch({
    type: ADD_COMMENT,
    payload: comment
  });
};
